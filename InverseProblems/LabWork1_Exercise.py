# -*- coding: utf-8 -*-

# Библиотеки
import numpy as np
from scipy.optimize import minimize  # rosen, rosen_der

# Настройка вывода матрицы на экран
np.set_printoptions(precision=5)

# Размер матрицы A
M = 10
N = 10

# Загрузка данных из файла
A = np.load('A.npy')
z = np.load('z.npy')
u = np.load('u.npy')

# Возмущённые значения epsilon_A, epsilon_u
epsilon_A = 1.0e-1
epsilon_u = 1.0e-1

# Возмущенные матрица А и правая часть u
A_var = A.copy()
z_var = z.copy()
u_var = u.copy()

for row in range(0, M):
    # Возмущение матрицы A
    for column in range(0, N):
        theta = 2.0 * np.random.random_sample() - 1.0
        A_var[row, column] = A[row, column] * (1.0 + theta * epsilon_A)

    # Возмущение правой части u
    theta = 2.0 * np.random.random_sample() - 1.0
    u_var[row] = u[row] * (1.0 + theta * epsilon_u)

# Задание параметров h и delta
h = epsilon_A * np.linalg.norm(A)
sigma = epsilon_u * np.linalg.norm(u)


# Задание вспомогательного функционала L_delta(z)
def func_l_delta(z_temp):
    
    # Норма ||A_h z - u_delta||
    first_add = 0.0
    for row0 in range(0, M):
        
        # Произведение A_h z
        az_sum = 0.0
        for column0 in range(0, N):
            az_sum = az_sum + A_var[row0, column0] * z_temp[column0]
        
        first_add = first_add + pow(az_sum - u_var[row0], 2.0)
    
    # Второе слагаемое h ||z||
    second_add = 0.0
    for column0 in range(0, N):
        second_add = second_add + pow(z_temp[column0], 2.0)
    
    # Итоговое значение
    func = pow(first_add, 0.5) + h * pow(second_add, 0.5) + sigma
    
    return func


# Минимизация функционала L_delta(z)
z0 = np.ones(N)
res = minimize(func_l_delta, z0, method='Powell')
print("Lambda_delta: ", res.fun)
lambda_delta = res.fun


# Расчёт обобщённой невязки
def rho_alpha(z_temp):
    
    # Норма ||A_h z - u_delta||^2
    first_add = 0.0
    for row0 in range(0, M):
        
        # Произведение A_h z
        az_sum = 0.0
        for column0 in range(0, N):
            az_sum = az_sum + A_var[row0, column0] * z_temp[column0]
        
        first_add = first_add + pow(az_sum - u_var[row0], 2.0)
    
    # Второе слагаемое h ||z||
    second_add = 0.0
    for column0 in range(0, N):
        second_add = second_add + pow(z_temp[column0], 2.0)
       
    # Итоговое решение
    func = first_add - pow(lambda_delta + sigma + h * pow(second_add, 0.5), 2.0)
    
    return func


# Расчёт решения уравнения Эйлера
def euler_solution(alpha):
    euler_matrix = alpha * np.identity(N) + A_var.transpose() @ A_var
    euler_right_part = A_var.transpose() @ u_var
    euler_z = np.linalg.solve(euler_matrix, euler_right_part)

    return euler_z


# Метод деления отрезка пополам
left_border = 1.0E-10
right_border = 1.0E10

f_left = rho_alpha(euler_solution(left_border))
f_right = rho_alpha(euler_solution(right_border))

epsilon = 1.0E-5
middle = 0.0
while abs(f_left - f_right) > epsilon:
    
    # Расчёт невязки в середине отрезка
    middle = (left_border + right_border) * 0.5
    f_middle = rho_alpha(euler_solution(middle))
    
    # Попадание в левый отрезок
    if f_left * f_middle < 0:
        right_border = middle
        f_right = f_middle

    # Попадание в правый отрезок
    if f_middle * f_right < 0:
        left_border = middle
        f_left = f_middle


# Расчёт меры несовместности
def mu_ea_eu(z_temp):
    # Норма ||A_h z - u_delta||
    first_add = 0.0
    for row0 in range(0, M):

        # Произведение A_h z
        az_sum = 0.0
        for column0 in range(0, N):
            az_sum = az_sum + A_var[row0, column0] * z_temp[column0]

        first_add = first_add + pow(az_sum - u_var[row0], 2.0)

    # Итоговое решение
    func = pow(first_add, 0.5)

    return func


# Минимизация функционала mu_ea_eu(z)
z0 = np.ones(N)
res = minimize(mu_ea_eu, z0, method='Powell')
print("mu_ea_eu: ", res.fun)

print("Alpha: ", middle)
print("Rho_alpha: ", rho_alpha(euler_solution(middle)))
print("||z - z_alpha||: ", np.linalg.norm(np.load('z.npy') - euler_solution(middle)))

np.savetxt('z_original.out', z, delimiter=',')
np.savetxt('1-1_z_alpha.out', euler_solution(middle), delimiter=',')
