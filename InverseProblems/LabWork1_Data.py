# -*- coding: utf-8 -*-

# Библиотеки
import numpy as np

# Настройка вывода матрицы на экран
np.set_printoptions(precision=3)

# Размер матрицы A
N = 10

# Генерация матрицы A
# A = np.identity(N)
A = np.random.rand(N, N)
for diag in range(0, N):
    A[diag, diag] = A[diag, diag] + 1.0

print("A: ", A)
print("Det: ", np.linalg.det(A))

# Генерация точного решения
z = np.random.rand(N)
print("z: ", z)

# Генерация правой части
u = A @ z
print("u: ", u)

# Сохранение данных в файл
np.save('A', A)
np.save('Z', z)
np.save('U', u)
