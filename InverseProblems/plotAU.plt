reset

set terminal wxt font "Times New Roman,16"
set key right bottom

set title "Случай {/Symbol e}_A = {/Symbol e}_U"
set xlabel "Номер элемента U_i"
set ylabel "Значение U_i"

plot '4-4_z_alpha.out' using 1 with lines lw 3 lc 1 ti "z^{/Symbol a} при {/Symbol e}_A = {/Symbol e}_U = 10^{-4}", \
     '3-3_z_alpha.out' using 1 with lines lw 3 lc 2 ti "z^{/Symbol a} при {/Symbol e}_A = {/Symbol e}_U = 10^{-3}", \
     '2-2_z_alpha.out' using 1 with lines lw 3 lc 3 ti "z^{/Symbol a} при {/Symbol e}_A = {/Symbol e}_U = 10^{-2}", \
     '1-1_z_alpha.out' using 1 with lines lw 3 lc 4 ti "z^{/Symbol a} при {/Symbol e}_A = {/Symbol e}_U = 10^{-1}", \
     'z_original.out'  using 1 with lines lw 3 lc -1 ti "Исходное решение"