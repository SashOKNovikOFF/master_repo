#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <windows.h>

// Перевод из перестановки в строку
void change_to_string(const std::vector<int>& input) {
	
	std::vector<char> alphabet(26);
	for (int i = 0; i < 26; i++)
		alphabet[i] = (char)(97 + i);

	for (int i = 0; i < input.size(); i++)
		std::cout << alphabet[input[i] - 1] << " ";
	std::cout << std::endl;

};

// Перевод из строки в перестановку
std::vector<int> string_to_change(const std::string& str) {
	
	std::vector<int> input(str.size());
	for (int i = 0; i < str.size(); i++)
		input[i] = (int)str[i] - 97 + 1;

	return input;

};

// Вывод вектора чисел
void output_vector(const std::vector<int>& input) {

	for (std::size_t i = 0; i < input.size(); i++)
		std::cout << input[i] << " ";
	std::cout << std::endl;

};

// 1. По текущей перестановке получить следующую
void from_to(std::vector<int>& input) {

	int N = (int)input.size();

	// Нашли элемент, который хотим поменять
	int changed_ind = N - 2;
	for (int i = N - 2; i >= 0; i--)
		if (input[i] < input[i + 1]) {
			changed_ind = i;
			break;
		};

	// Если дошли до конца, выходим из функции
	if (changed_ind == 0)
		return;

	// Нашли элемент, на который хотим поменять
	int new_place = changed_ind + 1;
	for (int i = changed_ind + 1; i < N; i++)
		if ((input[i] > input[changed_ind]) && (input[i] < input[new_place]))
			new_place = i;

	// Поменяли элементы местами
	std::swap(input[changed_ind], input[new_place]);

	// Повернули конец ряда
	std::reverse(input.begin() + changed_ind + 1, input.end());

};

// 2. По текущей перестановке получить предыдущую
void to_from(std::vector<int>& input) {

	int N = (int)input.size();

	// Нашли элемент, который хотим поменять
	int changed_ind = 0;
	for (int i = 0; i < N - 1; i++)
		if (input[i] > input[i + 1]) {
			changed_ind = i;
			break;
		};

	// Если дошли до конца, выходим из функции
	if (changed_ind == 0)
		return;

	// Нашли элемент, на который хотим поменять
	int new_place = changed_ind + 1;
	for (int i = changed_ind + 1; i < N; i++)
		if ((input[i] < input[changed_ind]) && (input[i] > input[new_place]))
			new_place = i;

	// Поменяли элементы местами
	std::swap(input[changed_ind], input[new_place]);

	// Повернули конец ряда
	std::reverse(input.begin() + changed_ind + 1, input.end());

};

// Факториал числа n
std::size_t factorial(std::size_t n) {

	std::size_t sum = 1;
	for (std::size_t i = 1; i <= n; i++)
		sum *= i;

	return sum;

};

// 3. По перестановке получить номер
std::size_t string_to(const std::vector<int> input) {

	std::vector<int> quantity(input.size(), 0.0);

	for (std::size_t i = 0; i < input.size(); i++)
	for (std::size_t j = i + 1; j < input.size(); j++)
	if (input[i] > input[j])
		quantity[i]++;

	std::size_t sum = 0;
	for (std::size_t i = 0; i < quantity.size() - 1; i++)
		sum += quantity[i] * factorial(quantity.size() - 1 - i);

	return sum;

};

// 4. По номеру получить перестановку
std::vector<int> number_to(long long int N, int number_) {

	int number = number_;
	int length = N;

	std::vector<int> input;

	// Словарь
	std::vector<int> dictionary;
	for (int i = 0; i < N; i++)
		dictionary.push_back(i + 1);

	// Пока номер перестановки положителен
	for (int i = 0; i < N; i++) {

		std::size_t number_permut = factorial(length - 1);

		// Предугадываем число в позиции

		std::size_t guess_number = number / number_permut;
		input.push_back(dictionary[guess_number]);
		dictionary.erase(dictionary.begin() + guess_number);

		// Переход на следующий шаг
		length--;
		number = number % number_permut;
	};

	return input;

};

int main() {

	setlocale(LC_ALL, "Russian");

	std::cout << "Выберите функцию, которую хотите использовать: " << std::endl;
	std::cout << "1 - Получить по текущей перестановке следующую;" << std::endl;
	std::cout << "2 - Получить по текущей перестановке предыдущую;" << std::endl;
	std::cout << "3 - Получить по номеру перестановку;" << std::endl;
	std::cout << "4 - Получить по перестановке её номер;" << std::endl;
	std::cout << "5 - Выход." << std::endl;

	std::cout << "Введите число: ";
	
	int menuPoint = 0;
	std::cin >> menuPoint;

	switch (menuPoint) {
	case 1:
		{
			std::cout << "Введите строку без пробелов: ";

			std::string str;
			std::cin >> str;
			std::vector<int> input = string_to_change(str);
			from_to(input);

			std::cout << "Следующая перестановка: ";
			change_to_string(input);

			break;
		}
	case 2:
		{
			  std::cout << "Введите строку без пробелов: ";

			  std::string str;
			  std::cin >> str;
			  std::vector<int> input = string_to_change(str);
			  to_from(input);

			  std::cout << "Предыдущая перестановка: ";
			  change_to_string(input);

			  break;
		}
	case 3:
		{
			std::cout << "Введите длину и номер перестановки: ";
		
			int length, number;
			std::cin >> length >> number;
			
			std::cout << "Перестановка: ";
			change_to_string(number_to(length, number));

			break;
		}
	case 4:
		{
			std::cout << "Введите строку без пробелов: ";

			std::string str;
			std::cin >> str;
			
			std::cout << "Номер перестановки: ";
			std::cout << string_to(string_to_change(str)) << std::endl;

			break;
		}
	case 5:
		std::cout << "Выход из программы..." << std::endl;
		exit(EXIT_SUCCESS);
		break;
	default:
		std::cerr << "Вы выбрали неверный вариант" << std::endl;
		exit(EXIT_FAILURE);
	};

	return 0;
};