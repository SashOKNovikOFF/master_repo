#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <climits>
#include <random>

#include <Windows.h>

int minElement(int a, int b) {

	return (a < b) ? a : b;

};

// Вывод вектора чисел
void output_vector(const std::vector<int>& input) {

	for (std::size_t i = 0; i < input.size(); i++)
		std::cout << input[i] << " ";
	std::cout << std::endl;

};

// 1. Слияние подмассивов
void merge(std::vector<int>& array, int low, int middle, int high) {

	int left = low; // Начало левой части
	int right = middle + 1; // Начало правой части

	std::vector<int> temp(high); // Временный массив для хранения исходного массива

	// Копирование исходного массива во временный массив
	for (int copy = low; copy < high; copy++)
		temp[copy] = array[copy];

	// Слияние массив путём сортировки
	for (int index = low; index < high; index++) {
		if (left > middle)                 { array[index] = temp[right]; right++; } // Если левая часть "пуста", заполнять правой частью
		else if (right >= high)             { array[index] = temp[left]; left++; } // Если правая часть "пуста", заполнять левой частью
		else if (temp[right] < temp[left]) { array[index] = temp[right]; right++; } // Если правый элемент меньше левого, заполнять правой частью
		else                               { array[index] = temp[left]; left++; }; // Если левый элемент меньше правого, заполнять левой частью
	};

};

// 2. Восходящая сортировка слиянием
void MergeSort(std::vector<int>& array) {

	int arraySize = (int)array.size();
	for (int size = 1; size < arraySize; size *= 2) {
		for (int low = 0; low < arraySize - size; low += 2 * size)
			merge(array, low, low + size - 1, minElement(low + 2 * size, arraySize));

		std::cout << "Temp array ( " << size << "): ";
		output_vector(array);
	};

};

// Сортировка вставками
void InsertSort(std::vector<int>& array) {

	int arraySize = (int)array.size();
	for (int index = 0; index < arraySize; index++) {
		for (int next = index; (next > 0) && (array[next] < array[next - 1]); next--)
			std::swap(array[next], array[next - 1]);

		std::cout << "Temp array ( " << index << "): ";
		output_vector(array);
	};

};

// 1. Разбиение для быстрой сортировки
int partition(std::vector<int>& array, int low, int high) {

	int left = low; // Левый индекс просмотра
	int right = high; // Правый индекс просмотра
	int pivot = low; // Индекс сравниваемого элемента
	
	while (left <= right) { // Выполняем перестановку элементов до нахождения нужной позиции

		// Ищем элемент слева, который будет больше сравниваемого
		while (array[left] < array[pivot])
			left++;

		// Ищем элемент справа, который будет меньше сравниваемого
		while (array[right] > array[pivot])
			right--;

		// По необходимости меняем элементы местами
		if (left <= right) {
			std::swap(array[left], array[right]);
			left++;
			right--;
		};
	};

	return left; // Возвращаем найденный индекс
};

// 2. Быстрая сортировка
void QuickSort(std::vector<int>& array, int low, int high) {

	int arraySize = (int)array.size();
	if (low < high) {
		int middle = partition(array, low, high);

		std::cout << "Partition (" << middle + 1 << "): ";
		output_vector(array);

		QuickSort(array, low, middle - 1);

		std::cout << "QuickSort (" << low + 1 << ", " << middle << "): ";
		output_vector(array);

		QuickSort(array, middle, high);

		std::cout << "QuickSort (" << middle + 1 << ", " << high + 1 << "): ";
		output_vector(array);
	};

};

int main() {

	std::cout << "Choose sort: " << std::endl;
	std::cout << "1 - Insert sort;" << std::endl;
	std::cout << "2 - Merge sort;" << std::endl;
	std::cout << "3 - Quick sort;" << std::endl;
	std::cout << "4 - Exit." << std::endl;

	std::cout << "Enter the number: ";
	
	int menuPoint = 0;
	std::cin >> menuPoint;

	switch (menuPoint) {
	case 1:
		{

		std::cout << "Enter the size of array (it will be random array): ";
		int size;
		std::cin >> size;

		// Генерация целых чисел (стандартное распределение)
		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(0, 100);

		// Заполнение массива целыми числами
		std::vector<int> input(size);
		for (int& value : input)
			value = uni(rng);

		std::cout << "Array: ";
		output_vector(input);

		InsertSort(input);

		std::cout << "Array: ";
		output_vector(input);

		break;
		}
	case 2:
		{

		std::cout << "Enter the size of array (it will be random array): ";
		int size;
		std::cin >> size;

		// Генерация целых чисел (стандартное распределение)
		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(0, 100);

		// Заполнение массива целыми числами
		std::vector<int> input(size);
		for (int& value : input)
			value = uni(rng);

		std::cout << "Array: ";
		output_vector(input);

		MergeSort(input);

		std::cout << "Array: ";
		output_vector(input);

		break;
		}
	case 3:
		{

		std::cout << "Enter the size of array (it will be random array): ";
		int size;
		std::cin >> size;

		// Генерация целых чисел (стандартное распределение)
		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(0, 100);

		// Заполнение массива целыми числами
		std::vector<int> input(size);
		for (int& value : input)
			value = uni(rng);

		std::cout << "Array: ";
		output_vector(input);

		QuickSort(input, 0, (int)input.size() - 1);

		std::cout << "Array: ";
		output_vector(input);

		break;
		}
	case 4:
		std::cout << "Exit..." << std::endl;
		exit(EXIT_SUCCESS);
		break;
	default:
		std::cerr << "Wrong variant!" << std::endl;
		exit(EXIT_FAILURE);
	};

	return 0;
};