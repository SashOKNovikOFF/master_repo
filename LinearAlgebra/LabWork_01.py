import numpy as np
from time import time

# Настройка вывода матрицы на экран
np.set_printoptions(precision = 3)

# Считаем, что матрицы квадратные
N = 500

### Решение СЛАУ методом Гаусса (через "строки")
# A - матрица
# b - столбец свободных членов
def gauss_strings(A, b):

    if abs(np.linalg.det(A)) < 1.0E-6:
        return -1.0

    for column in range(0, N):
        main_elem = A[column, column]
        A[column, :] = A[column, :] / main_elem
        b[column] = b[column] / main_elem

        for row in range(column + 1, N):
            b[row] = b[row] - A[row, column] * b[column]
            A[row, :] = A[row, :] - A[row, column] * A[column, :]

    x_gen = np.zeros(N)
    x_gen[N - 1] = b[N - 1]
    for row in range(N - 2, -1, -1):
        x_gen[row] = b[row] - A[row, (row + 1):] @ x[(row + 1):]

    return x_gen


### Решение СЛАУ методом Гаусса (через циклы)
# A - матрица
# b - столбец свободных членов
def gauss_cycles(A, b):

    if abs(np.linalg.det(A)) < 1.0E-6:
        return -1.0

    for column in range(0, N):
        main_elem = A[column, column]

        for temp in range(0, N):
            A[column, temp] = A[column, temp] / main_elem
        #A[column, :] = A[column, :] / main_elem

        b[column] = b[column] / main_elem

        for row in range(column + 1, N):
            b[row] = b[row] - A[row, column] * b[column]
            for temp in range(column + 1, N):
                A[row, temp] = A[row, temp] - A[row, column] * A[column, temp]
            A[row, column] = 0.0

    x_gen = np.zeros(N)

    for row in range(N - 1, -1, -1):
        x_gen[row] = b[row]
        for column in range(row + 1, N, 1):
            x_gen[row] = x_gen[row] - x[column] * A[row, column]

    return x_gen

A = np.random.rand(N, N)
x = np.random.rand(N)
b = A @ x
# print("X: ", x)
# print("X_0 (strings): ", gauss_strings(A, b))
# print("X_0 (cycles): ", gauss_cycles(A, b))
# print("Error: ", np.linalg.norm(x - gauss_strings(A, b)))
# print("Error: ", np.linalg.norm(x - gauss_cycles(A, b)))

# Замеры
M = 1

tic = time()
step = 0
while (step < M):
    gauss_strings(A, b)
    step = step + 1
toc = time()
print("Gauss strings: ", toc - tic)
strings_time = toc - tic

tic = time()
step = 0
while (step < M):
    gauss_cycles(A, b)
    step = step + 1
toc = time()
print("Gauss cycles: ", toc - tic)
cycles_time = toc - tic

print("Time: ", abs(cycles_time - strings_time) / cycles_time * 100, "%")

