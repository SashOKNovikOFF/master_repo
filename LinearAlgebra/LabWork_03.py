import numpy as np
import matplotlib.pyplot as plt
from time import time


# Функция-декоратор для замеров работы других функций
def time_decorator(calc_function):

    def wrapper_function(arg1, arg2):
        tic = time()  # Начало замера
        calc_function(arg1, arg2)  # Вызовем функцию, время выполнения которой необходимо рассчитать
        tac = time()  # Конец замера

        print("Time: ", tac - tic)  # Время расчёта

    return wrapper_function


N = 1000  # Размер квадратной матрицы
k = 0.5  # Коэффициент для улучшения сходимости
err_norm = 1.0e-6  # Граница относительной нормы погрешности
err_mach = 1.0e-8  # Граница машинного эпсилона
err_conj = 1.0e-2  # Граница 'машинного' эпсилона для МСГ
PG_flag = False  # Включение проверки условия Петрова-Галёркина
FUNC_flag = False  # Включение проверки уменьшения значения функционала F(x)
CONV_flag = False  # Включение вывода по уменьшению невязки для каждого метода
VEL_flag = True  # Включение сравнения скорости убывания невязки с теор. оценки для ММН
CALC_flag = False  # Флаг, изменяющий способ расчёта невязки в МНС

r_vec_1 = []  # Вектор относительных невязок
r_vec_2 = []  # Вектор относительных невязок
r_vec_3 = []  # Вектор относительных невязок
r_vec = []  # Вектора невязок
p_vec = []  # Вектора направлений
RP_flag = False  # Вывод графики изменения относительной невязки


# Сгенерировать симметрическую положительно определённую матрицу A = B^T * B + k * E
def generate_matrix():
    b = np.random.rand(N, N)
    b = b[:, :] - 0.5
    return b @ b.transpose() + k * np.identity(N)


# Метод наискорейшего спуска
@time_decorator
def steepest_descent_alg(matrix, vector):
    x_old = np.ones(N)
    x_new = np.ones(N)

    r = vector - matrix @ x_old
    p = matrix @ r
    conv = 1.0

    # 5. Расчёт функционала
    old_func0 = x_old.dot(matrix @ x_old) - 2 * vector.dot(x_old)

    iteration = 0  # Подсчёт номера итерации

    while conv >= err_norm:  # Пока не обеспечивается сходимость, выполняем метод
        x_old = x_new
        r_vec_1.append(np.linalg.norm(r, 2))

        denom = p.dot(r)
        if denom < err_mach*1e-5:
            raise ValueError("Divide by zero!", denom)

        alpha = r.dot(r) / denom
        x_new = x_old + alpha * r

        # 4. Проверка условия Петрова-Галёркина
        if PG_flag and abs(r.dot(vector - matrix @ x_new)) > err_mach:
            print("Error in Petrov-Galerkin condition: ", r.dot(vector - matrix @ x_new))
            break

        # 5. Проверка уменьшения значения функционала F(x) = (A * x, x) - 2 * (b * x, x)
        if FUNC_flag:
            old_func1 = x_new.dot(matrix @ x_new) - 2 * vector.dot(x_new)
            if old_func1 - old_func0 > err_mach:
                print("Error in functional minimizing: ", old_func1 - old_func0)
                break
            else:
                old_func0 = old_func1

        # 6. Сравнение результата при изменении расчёта невязки
        if CALC_flag:
            r = r - alpha * p
        else:
            r = vector - matrix @ x_new

        p = matrix @ r

        conv = np.linalg.norm(x_new - x_old, 2) / np.linalg.norm(x_new, 2)

        iteration += 1

        if CONV_flag:
            print_residual = np.linalg.norm(b_vector - a_matrix @ x_new, 2)  # Норма невязки
            print_rel_residual = print_residual / np.linalg.norm(b_vector, 2)  # Относительная норма невязки
            print("res = %10.8f, rel_res = %10.8f, iter = %d" % (print_residual, print_rel_residual, iteration))

    return x_new

def cosAngle(v1,v2):
    return v1.dot(v2)/np.linalg.norm(v1)/np.linalg.norm(v2)

# Метод минимальной невязки
def minimal_residual_iteration(matrix, vector):
    x_old = np.ones(N)
    x_new = np.ones(N)

    r = vector - matrix @ x_old
    p = matrix @ r
    conv = 1.0

    iteration = 0  # Подсчёт номера итерации

    while conv >= err_norm:  # Пока не обеспечивается сходимость, выполняем метод
        x_old = x_new
        r_vec_2.append(np.linalg.norm(r, 2))

        denom = p.dot(p)
        if denom < err_mach*1e-5:
            raise ValueError("Divide by zero!", denom)

        alpha = p.dot(r) / denom
        x_new = x_old + alpha * r

        # 8. Проверка условия Петрова-Галёркина
        if PG_flag and abs(cosAngle(matrix @ r , vector - matrix @ x_new)) > err_mach:
            print(str(iteration)+": Error in Petrov-Galerkin condition: ", cosAngle(matrix @ r , vector - matrix @ x_new))
            break

        r = r - alpha * p
        p = matrix @ r

        conv = np.linalg.norm(x_new - x_old, 2) / np.linalg.norm(x_new, 2)

        iteration += 1

        if CONV_flag:
            print_residual = np.linalg.norm(b_vector - a_matrix @ x_new, 2)  # Норма невязки
            print_rel_residual = print_residual / np.linalg.norm(b_vector, 2)  # Относительная норма невязки
            print("res = %10.8f, rel_res = %10.8f, iter = %d" % (print_residual, print_rel_residual, iteration))

    return x_new


# Метод сопряжённых градиентов
def conjugate_gradient(matrix, vector):
    x_old = np.ones(N)
    x_new = np.ones(N)

    r_old = vector - matrix @ x_old
    r_new = vector - matrix @ x_new
    p = r_old
    conv = 1.0

    iteration = 0  # Подсчёт номера итерации

    while conv >= err_norm:  # Пока не обеспечивается сходимость, выполняем метод
        x_old = x_new
        r_old = r_new.copy()

        r_vec_3.append(np.linalg.norm(r_old, 2))

        # 14. Запоминаем величины r и p на текущем шаге
        r_vec.append(r_old)
        p_vec.append(p)

        alpha = r_old.dot(r_old) / (matrix @ p).dot(p)
        # alpha = r_old.dot(p) / p.dot(matrix @ p)  # 15. Замена формулы alpha

        x_new = x_old + alpha * p
        r_new = r_old - alpha * matrix @ p

        beta = r_new.dot(r_new) / r_old.dot(r_old)
        # beta = - (matrix @ r_new).dot(p) / p.dot(matrix @ p)  # 15. Замена формулы beta

        p = r_new + beta * p

        conv = np.linalg.norm(x_new - x_old, 2) / np.linalg.norm(x_new, 2)

        iteration += 1

        if CONV_flag:
            print_residual = np.linalg.norm(b_vector - a_matrix @ x_new, 2)  # Норма невязки
            print_rel_residual = print_residual / np.linalg.norm(b_vector, 2)  # Относительная норма невязки
            print("res = %10.8f, rel_res = %10.8f, iter = %d" % (print_residual, print_rel_residual, iteration))

    return x_new


# Расчёт скорости убывания невязки для метода минимальной невязки
def get_minimizing_velocity(matrix):

    sigma = np.linalg.norm(matrix, 2)
    eig_values = np.linalg.eigvals(matrix)
    mu = min(eig_values)

    return pow(1.0 - mu * mu / sigma / sigma, 2.0)


# Вывод данных по отклонению численного решения от точного решения
def print_error_result(solution, x):
    print_error = np.linalg.norm(solution - x, 2)
    print_rel_error = print_error / np.linalg.norm(solution, 2)
    print("Норма отклонения от точного решения: ", print_error)
    print("Относительное отклонение решения: ", print_rel_error)


# 6. Проверка скорости работы программы МНС при различном расчёте невязок
def func_6_check_velocity(matrix, vector):
    global CALC_flag

    CALC_flag = True
    steepest_descent_alg(matrix, vector)
    CALC_flag = False
    steepest_descent_alg(matrix, vector)


# 10. Сравнение скорости убывания невязки с теор. оценкой
def func_10_diff_vel():
    q = get_minimizing_velocity(a_matrix)

    for ind in range(1, len(r_vec_2)):
        old_norm_r = np.linalg.norm(r_vec_2[ind - 1])
        new_norm_r = np.linalg.norm(r_vec_2[ind])
        print("||r_k|| / ||r_{k + 1}|| = %f, q = %f" % (new_norm_r / old_norm_r, q))


def func_14_check_dot():
    # 14. Проверка (Ap_i, p_j) = 0
    for i in range(0, len(p_vec)):
        for j in range(0, len(p_vec)):
            val = (a_matrix @ p_vec[i]).dot(p_vec[j])

            if (i != j) and (val > err_conj):
                raise ValueError("(Ap_%d, p_%d) not equal 0!" % (i, j), val)

    # 14. Проверка (r_i, r_j) = 0
    for i in range(0, len(r_vec)):
        for j in range(0, len(r_vec)):
            val = r_vec[i].dot(r_vec[j])

            if (i != j) and (val > err_conj):
                raise ValueError("(r_%d, r_%d) not equal 0!" % (i, j), val)


# 1. Генерация матрицы A = B^T * B + k * E
a_matrix = generate_matrix()

# Формирование решения и правой части
x_vector = np.random.rand(N)
b_vector = a_matrix @ x_vector

# 2. Реализация метода наискорейшего спуска
# solution_1 = steepest_descent_alg(a_matrix, b_vector)
# print_error_result(solution_1, x_vector)

# 6. Проверить скорость работы программы МНС при различном расчёте невязок
func_6_check_velocity(a_matrix, b_vector)

# 7. Реализация метода наименьших невязок
solution_2 = minimal_residual_iteration(a_matrix, b_vector)
print_error_result(solution_2, x_vector)

func_10_diff_vel()
exit()

# 11. Реализация метода сопряжённых градиентов
solution_3 = conjugate_gradient(a_matrix, b_vector)
print_error_result(solution_3, x_vector)

# 3. 9. График изменения относительной невязки

if RP_flag:

    dots_1 = list(range(0, len(r_vec_1)))
    dots_2 = list(range(0, len(r_vec_2)))
    dots_3 = list(range(0, len(r_vec_3)))
    element_1 = plt.semilogy(dots_1, r_vec_1, label="Метод скорейшего спуска")
    element_2 = plt.semilogy(dots_2, r_vec_2, label="Метод минимальных невязок")
    element_3 = plt.semilogy(dots_3, r_vec_3, label="Метод сопряжённых градиентов")
    plt.xlabel("Number of iteration")
    plt.ylabel("Norm of residual")
    plt.legend()

    plt.show()

# 10. Сравнение скорости убывания невязки с теор. оценкой
# func_10_diff_vel()

# 14. Проверка (Ap_i, p_j) = 0 и (r_i, r_j) = 0
# func_14_check_dot()
