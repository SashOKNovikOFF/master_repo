import numpy as np
import scipy.linalg as sl
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math

# TODO: Неправильное отображение собственного вектора в 1-ом и 4-ом графиках

# При печати отображаем 3 знака после запятой, 7 элементов в матрице по краям до многоточий
np.set_printoptions(edgeitems=7, precision=3)
# широкие строки в браузере для широкого экрана
np.core.arrayprint._line_width = 200

n = 41  # Размер квадратной матрицы
length = 1.0  # Размер области
dx = length / (n - 1)  # Размер ячейки сетки
P = np.zeros((n, n), dtype=np.int32)  # Матрица нулей и единиц, по которой задаются границы области уравнения Лапласа
Nums = np.zeros(P.shape, dtype=np.int32) - 1  # Нумерация элементов по матрице нулей и единиц
plot_flag = False  # Вывод графиков собственных векторов в виде z(x, y)
plot_num_flag = True  # Вывод графиков (численных) собственных векторов в виде z(x, y)
eps = 1.0e-12  # Сходимость

# Добавить область "прямоугольник"
def add_rectangle(p, x0, y0, x1, y1):
    i0 = round(x0 * (n - 1))
    j0 = round(y0 * (n - 1))
    i1 = round(x1 * (n - 1))
    j1 = round(y1 * (n - 1))
    p[i0:i1, j0:j1] = 1


# Добавить область "эллипс"
def add_ellipse(p, x0, y0, r1, r2):

    for i in range(n):
        for j in range(n):
            x = i / n
            y = j / n
            if (x - x0) * (x - x0) / r1 / r1 + (y - y0) * (y - y0) / r2 / r2 <= 1:
                p[i, j] = 1


# Пронумеровать элементы по матрице нулей и единиц
def create_num_matrix(p, nums):
    k = 0  # Текущий номер переменной

    for i in range(n):
        for j in range(n):
            if p[i, j] == 1:
                nums[i, j] = k
                k += 1

    return k


# Составить матрицу для решения уравнения Лапласа
def get_lapl_matrix(matrix):
    for i in range(0, n):
        for j in range(0, n):
            # Если значение не принадлежит области, пропускаем его
            if Nums[i, j] == -1:
                continue

            k = Nums[i, j]
            matrix[k, k] = -4.0

            # Заполняем матрицу
            if Nums[i + 1, j] != -1:
                matrix[k, Nums[i + 1, j]] = 1.0
            if Nums[i - 1, j] != -1:
                matrix[k, Nums[i - 1, j]] = 1.0
            if Nums[i, j - 1] != -1:
                matrix[k, Nums[i, j - 1]] = 1.0
            if Nums[i, j + 1] != -1:
                matrix[k, Nums[i, j + 1]] = 1.0

    matrix[:, :] = matrix[:, :] / dx / dx


# Представить собственный вектор в виде поверхности z(x, y)
def vector_to_grid(x, nums):
    u = np.zeros(nums.shape)

    for i in range(0, n):
        for j in range(0, n):
            if nums[i, j] == -1:
                u[i, j] = 0
            else:
                u[i, j] = x[nums[i, j]]

    return u


# Сравнить собственные числа с аналитическими формулами
def diff_num_analytic(eigen_values):
    temp = []
    for row in range(1, 11):
        for col in range(1, 11):
            temp.append(- pow(math.pi, 2.0) * (row * row + col * col))

    temp.sort(reverse=True)
    print("Numerical lambda_min: ", eigen_values[0])
    print("Analytical lambda_min: ", temp[0])
    print("Difference: ", abs(eigen_values[0] - temp[0]))


# Рассчитать минимальное собственное значение методом обратных итераций
def calc_lambda_min(a_matrix, m0, translate=False):
    x_new = np.zeros(m0)
#    x_new[-5:-1]=1
#    x_new = np.ones(m0)
    x_new[10:200]=1
    e_matrix = np.identity(m0)

    lambda_old = 1.0
    lambda_new = 0.0
    iteration = 0

    while np.linalg.norm(lambda_old - lambda_new) > eps:

        lambda_old = lambda_new
        x_old = x_new.copy()

        if translate:
            y = np.linalg.solve(a_matrix - e_matrix * lambda_old, x_old)
        else:
            y = np.linalg.solve(a_matrix, x_old)
        norm_y = np.linalg.norm(y)
        x_new = y.copy() / norm_y

        # Отношение Рэлея
        lambda_new = x_new.dot(a_matrix.dot(x_new)) / x_new.dot(x_new)
        #if translate: lambda_new += x_old[10] / y[10] + lambda_old

        iteration += 1

    return lambda_new, x_new, iteration


# Найти собственное число, ближайшее к заданному k
def find_lambda_k(k0, matrix, m0):
    matrix = matrix - np.identity(m0) * k0
    lambda_k0, vector_k0, iteration_k0 = calc_lambda_min(matrix, m0)
    lambda_k0 += k0
    print("k: ", k0)
    print("lambda: ", lambda_k0)
    print("vector: ", vector_k0)

    return lambda_k0, vector_k0, iteration_k0


# Добавить область в виде прямоугольника и эллипса
add_rectangle(P, 1/n, 1/n, 1.0, 1.0)
# add_ellipse(P, 0.5, 0.5, 0.2, 0.4)

# Пронумеровать элементы по матрице нулей и единиц
m = create_num_matrix(P, Nums)  # Число ненулевых элементов

# По заданной области составить матрицу для решения уравнения Лапласа
A_matrix = np.zeros((m, m))
get_lapl_matrix(A_matrix)

# 1. Вычислить собственные числа и вектора стандартными средствами
start = time.clock()
eig_vals, eig_vectors = np.linalg.eig(A_matrix)
end = time.clock()
print("Time (eig, NumPy): ", end - start)

# start = time.clock()
# eigh_vals, eigh_vectors = np.linalg.eigh(A_matrix)
# end = time.clock()
#
# print("Time: ", end - start)

# 2. Отсортировать собственные значения по убыванию

idx = eig_vals.argsort()[::-1]
eig_vals = eig_vals[idx]
eig_vectors = eig_vectors[:, idx]

print("Check (NumPy): ", np.linalg.norm(A_matrix @ eig_vectors[:, 0] - eig_vals[0] * eig_vectors[:, 0]))

# 3-4. Изобразить выбранные собственные вектора в виде поверхности z(x, y)
if plot_flag:
    X = np.arange(0, length + dx, dx)
    Y = np.arange(0, length + dx, dx)
    X, Y = np.meshgrid(X, Y)
    Z_0 = vector_to_grid(eig_vectors[:, 0], Nums)
    Z_1 = vector_to_grid(eig_vectors[:, 1], Nums)

    fig1 = plt.figure(figsize=(19, 9))
    sub1 = fig1.add_subplot(121, projection='3d')
    sub1.set_title("Lambda = " + str(eig_vals[0]))
    sub1.plot_surface(X, Y, Z_0)
    sub1.set_zlim(-0.2, 0.2)

    sub2 = fig1.add_subplot(122, projection='3d')
    sub2.set_title("Lambda = " + str(eig_vals[1]))
    sub2.plot_surface(X, Y, Z_1)
    sub2.set_zlim(-0.2, 0.2)

    plt.show()

# 5. Сравнить собственные числа с аналитическими формулами
# diff_num_analytic(eig_vals)

# 6. Рассчитать минимальное собственное значение методом обратных итераций
start = time.clock()
lambda_1, vector_1, iteration_1 = calc_lambda_min(A_matrix, m)
end = time.clock()
print("Time (calc_lambda_min, Method): ", end - start)

print("Check_1: ", np.linalg.norm(A_matrix @ vector_1 - lambda_1 * vector_1) / np.linalg.norm(vector_1))

# 7. Найти собственное число, ближайшее к заданному k
# k = -5.0
# lambda_k, vector_k, iteration_k = find_lambda_k(k, A_matrix, m)

# 8-а). Смещение матрицы на собственное число
start = time.clock()
lambda_ofs_1, vector_ofs_1, iteration_ofs_1 = calc_lambda_min(A_matrix, m, True)
end = time.clock()
print("Time (calc_lambda_min, offset, Method): ", end - start)

print("Check_ofs_1: ", np.linalg.norm(A_matrix @ vector_ofs_1 - lambda_ofs_1 * vector_ofs_1))

# 8-б). Приведение матрицы Хессенберга
A_hes = sl.hessenberg(A_matrix)

start = time.clock()
lambda_h_1, vector_h_1, iteration_h_1 = calc_lambda_min(A_hes, m)
end = time.clock()
print("Time (calc_lambda_min, Hes, Method): ", end - start)

print("Check_h_1: ", np.linalg.norm(A_hes @ vector_h_1 - lambda_h_1 * vector_h_1))

print("Lambda_min (NumPy): ", eig_vals[0])
print("Lambda_min (Method): ", lambda_1)
print("Lambda_min, offset (Method): ", lambda_ofs_1)
print("Lambda_min (Hess): ", lambda_h_1)

print("Iteration (Method): ", iteration_1)
print("Iteration, offset (Method): ", iteration_ofs_1)
print("Iteration (Hess): ", iteration_h_1)

print("Vectors error (NumPy, Method): ", np.linalg.norm(eig_vectors[:, 0] - vector_1))
print("Vectors error (NumPy, offset): ", np.linalg.norm(eig_vectors[:, 0] - vector_ofs_1))

# 9. Построить графики собственных векторов
if plot_num_flag:
    X = np.arange(0, length + dx, dx)
    Y = np.arange(0, length + dx, dx)
    X, Y = np.meshgrid(X, Y)
    Z_0 = vector_to_grid(eig_vectors[:, 0], Nums)
    Z_1 = vector_to_grid(vector_1, Nums)
    Z_2 = vector_to_grid(vector_ofs_1, Nums)

    fig1 = plt.figure(figsize=(20, 10))
    sub1 = fig1.add_subplot(131, projection='3d')
    sub1.set_title("Lambda (NumPy) = " + str(eig_vals[0]))
    sub1.plot_surface(X, Y, Z_0)

    sub2 = fig1.add_subplot(132, projection='3d')
    sub2.set_title("Lambda (Method) = " + str(lambda_1))
    sub2.plot_surface(X, Y, Z_1)

    sub3 = fig1.add_subplot(133, projection='3d')
    sub3.set_title("Lambda (Offset) = " + str(lambda_ofs_1))
    sub3.plot_surface(X, Y, Z_2)

    plt.show()
