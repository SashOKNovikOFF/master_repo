import numpy as np
from scipy.linalg import solve_triangular
from time import time

# TODO: возможно ли транспонировать вектор [a, b, c] без использования функции reshape('размер_вектора', 1)?
# TODO: какие собственные числа должны быть у матрицы отражений?

# Настройка вывода матрицы на экран
np.set_printoptions(precision=3)

# Задание данных
matrixSize = 3
checkFlag = False


# Построение элементарной матрицы отражений U(x) = E - 2 * x * x^T
def householder_matrix(_x):
    if abs(np.linalg.norm(_x) - 1.0) > 1.0e-7:
        raise ValueError("Norm of vector is not equal 1!")

    x_trans = _x.reshape(matrixSize, 1).transpose()  # Вектор-строка x
    identity_matrix = np.identity(matrixSize)  # Единичная матрица
    return identity_matrix - 2 * _x.reshape(matrixSize, 1) @ x_trans  # Матрица U(x)


# Проверка ортогональности, симметричности, и собственных чисел U(x)
def check_householder_matrix(_reflection_matrix):
    # Проверка ортогональности U(x)
    print("Matrix is orthogonal: ", _reflection_matrix @ _reflection_matrix.transpose())

    # Проверка симметричности U(x)
    if np.array_equal(_reflection_matrix, _reflection_matrix.transpose()):
        print("Matrix is equal!")

    # Проверка собственных чисел
    print("Eigen: ", np.linalg.eig(_reflection_matrix))


# Функция-декоратор для замеров работы других функций
def time_decorator(calc_function):

    def wrapper_function(arg1, arg2):
        tic = time()  # Начало замера
        calc_function(arg1, arg2)  # Вызовем функцию, время выполнения которой необходимо рассчитать
        tac = time()  # Конец замера

        print("Time: ", tac - tic)  # Время расчёта

    return wrapper_function


# Неоптимизированное умножение матрицы U(x) на A
# @time_decorator
def not_optimized_u_mult_a(u_mat, a_mat):
    return u_mat @ a_mat


# Оптимизированное умножение матрицы U(x) на A скалярным произведением
# @time_decorator
def optimized_u_mult_a(_x, a_mat):
    mult = _x.transpose().dot(a_mat)
    return a_mat - 2.0 * _x @ mult


# Оптимизированное умножение матрицы U(x) на модифицированную матрицу A
def optimized_dot_u_mult_a(_index, _x, a_mat):
    mult = np.zeros((1, matrixSize))
    mult[0, _index:] = _x[_index:].transpose().dot(a_mat[_index:, _index:])
    return a_mat - 2.0 * _x @ mult


# Применение матрицы отражений для зануления соответствующих элементов i-ого столбца
def reflection_index(_index, matrix, vector):

    y = matrix[:, _index].reshape((matrixSize, 1)).copy()  # Строка Ai модифицированной матрицы A
    y[:_index] = 0.0  # Зануляем вектор до index - 1 элемента
    norm_vec = np.linalg.norm(y)  # Рассчитываем норму модифицированного вектора Ai

    y[_index, 0] = y[_index, 0] - norm_vec  # Рассчитываем вектор y_i
    norm_y = np.linalg.norm(y)  # Рассчитываем норму вектора y_i
    x_ind = y / norm_y  # Рассчитываем вектор x_i

    # Рассчитываем произведение U(x_i) на A
    return optimized_dot_u_mult_a(_index, x_ind, matrix), optimized_u_mult_a(x_ind, vector)


# Получение верхнетреугольной матрицы R из матрицы A
def get_upper_triangle_matrix(matrix, vector):
    for i in range(0, matrixSize - 1):
        matrix, vector = reflection_index(i, matrix, vector)

    return matrix, vector


# Решение системы Ax = b при верхнетреугольной матрице A
def get_solution_triangular(matrix, vector):
    solution = np.zeros((matrixSize, 1))

    for i in range(matrixSize - 1, -1, -1):
        # TODO: произведение матриц или скалярное произведение векторов?
        _sum = matrix[i, i + 1:matrixSize] @ solution[i + 1:matrixSize]
        solution[i] = (vector[i] - _sum) / matrix[i, i]

    return solution


# Решение системы Ax = b средствами NumPy
@time_decorator
def library_method(matrix, vector):
    return np.linalg.solve(matrix, vector)


# Решение системы Ax = b методом отражений
@time_decorator
def reflection_method(matrix, vector):
    _R, _new_b = get_upper_triangle_matrix(matrix, vector)
    return get_solution_triangular(_R, _new_b)


# Оптимизированное умножение матрицы Q(x) скалярным произведением
# @time_decorator
def optimized_q_mult(_x, q_mat):
    mult = q_mat.dot(_x)
    return q_mat - 2.0 * mult @ _x.transpose()


# Применение матрицы отражений для зануления соответствующих элементов i-ого столбца
def reflection_q_r_index(_index, matrix, vector, _q):

    y = matrix[:, _index].reshape((matrixSize, 1)).copy()  # Строка Ai модифицированной матрицы A
    y[:_index] = 0.0  # Зануляем вектор до index - 1 элемента
    norm_vec = np.linalg.norm(y)  # Рассчитываем норму модифицированного вектора Ai

    y[_index, 0] = y[_index, 0] - norm_vec  # Рассчитываем вектор y_i
    norm_y = np.linalg.norm(y)  # Рассчитываем норму вектора y_i
    x_ind = y / norm_y  # Рассчитываем вектор x_i

    # Рассчитываем произведение U(x_i) на A и Q
    return optimized_dot_u_mult_a(_index, x_ind, matrix), optimized_u_mult_a(x_ind, vector), optimized_q_mult(x_ind, _q)


# Получение матриц Q и R: A = QR
def get_q_r_matrices(matrix, vector):
    _q = np.identity(matrixSize)

    for i in range(0, matrixSize - 1):
        matrix, vector, _q = reflection_q_r_index(i, matrix, vector, _q)

    return matrix, vector, _q


# 1. Построение матрицы отражений U(x)
x = np.random.rand(matrixSize)
x = x / np.linalg.norm(x)
reflectionMatrix = householder_matrix(x)

# 1. Проверка матрицы отражений U(x)
if checkFlag:
    check_householder_matrix(reflectionMatrix)

# 2. Построение оптизимизированной процедуры домножения A на U(x)

A_Matrix = np.random.rand(matrixSize, matrixSize)  # Матрица A
solution_Vector = np.random.rand(matrixSize, 1)  # Вектор x
b_Vector = A_Matrix @ solution_Vector  # Вектор b

not_optimized_u_mult_a(reflectionMatrix, A_Matrix)
optimized_u_mult_a(x.reshape(matrixSize, 1), A_Matrix)

# 3. Реализация метода отражений

R, new_b = get_upper_triangle_matrix(A_Matrix, b_Vector)

# 4. Решение системы Ax = b методом отражений

ans_1 = solution_Vector - np.linalg.solve(A_Matrix, b_Vector)
ans_2 = solution_Vector - np.linalg.solve(R, new_b)
ans_3 = solution_Vector - get_solution_triangular(R, new_b)
ans_4 = solution_Vector - solve_triangular(R, new_b)

print("np.linalg.solve(A, b): ", np.linalg.norm(ans_1))
print("np.linalg.solve(R, new_b): ", np.linalg.norm(ans_2))
print("get_solution_triangular(R, new_b): ", np.linalg.norm(ans_3))
print("scipy.linalg.solve_triangular(R, new_b): ", np.linalg.norm(ans_4))

# 5. Проверка работы алгоритма при больших размерах матрицы

library_method(A_Matrix, b_Vector)
reflection_method(A_Matrix, b_Vector)

# 6. Модификация метода отражений с расчётом матрицы Q

R, new_b, Q = get_q_r_matrices(A_Matrix, b_Vector)

print("Norm A: ", np.linalg.norm(A_Matrix))
print("Norm QR: ", np.linalg.norm(Q @ R))

# 7. Сравнить полученный результат со стандартной функцией

Q_, R_ = np.linalg.qr(A_Matrix)
print("Norm Q_R_: ", np.linalg.norm(Q_ @ R_))
print("Q:\n", Q)
print("Q_:\n", Q_)
print("R:\n", R)
print("R_:\n", R_)
