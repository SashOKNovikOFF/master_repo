
# coding: utf-8

# In[333]:


import numpy.linalg as la

import scipy.sparse.linalg as spla

import scipy.sparse as sp
import numpy as np
import matplotlib.pyplot as plt
from time import time

np.set_printoptions(edgeitems=7,precision=3) # 3 знако после запятой, 7 элементов в матрице по краям до многоточий
np.core.arrayprint._line_width = 200 # широкие строки в браузере для широкого экрана

n=1000
get_ipython().magic('matplotlib notebook')


# In[334]:


A=sp.dok_matrix((n,n))
num = n*20
ts = time()
for k in range(num):
    i=np.random.randint(n)
    j=np.random.randint(n)
    A[i,j]=1;
    A[j,i]=1;
    

for i in range(n): A[i,i]=5
    
print( (time()-ts)*1000 / A.nnz )

    


# In[335]:


if n<100: A.toarray() 


# In[336]:


A.nnz


# In[337]:


plt.spy(A,markersize=0.3)
plt.show()


# In[354]:


x_exact=np.random.rand(n)


# In[355]:


b=A@x_exact


# In[356]:


get_ipython().magic('time la.norm( spla.spsolve(A,b)-x_exact )')




# In[357]:


get_ipython().magic('time A1=A.tocsr()')
get_ipython().magic('time la.norm( spla.spsolve(A1,b)-x_exact )')


# In[358]:


A1.data


# In[359]:


A1.indices


# In[360]:


A1.indptr


# In[361]:


x,info=spla.cg(A1,b,tol=1e-8,maxiter=1000)


# In[362]:


x-x_exact


# In[363]:


info


# In[364]:


def ff(x):
    print(la.norm(A1.dot(x)-b)/la.norm(b))


# In[372]:


get_ipython().magic('time x1,info= spla.cg(A1,b,tol=1e-8, callback=ff)')


# In[373]:


la.norm(x1-x_exact)

