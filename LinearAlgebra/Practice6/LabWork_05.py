import ReadHB_Func
import scipy as sp
import matplotlib.pyplot as plt
import scipy.sparse.linalg as sp_lg
import time
from numpy.linalg import norm

src_flag = False  # Вывод исходных данных по матрице
prec_flag = False  # Вывод времени работы расчёта предобуславливателей
plot_flag = False  # Вывод графика вида матрицы
plot_r_flag = False  # Вывод графика зависимости невязки от числа итераций

A = ReadHB_Func.readHB('MATRIX.DAT')  # Матрица A

# Вывести вид матрицы A
if plot_flag:
    plt.spy(A, markersize=0.1)
    plt.show()

# Исходные данные по матрице

if src_flag:
    print("Размер матрицы: ", A.shape[0])

    inv_A = sp_lg.inv(A)  # Обратная матрица
    norm_A = sp_lg.norm(A)  # Норма матрицы A
    norm_A1 = sp_lg.norm(inv_A)  # Норма обратной матрицы
    print("Число обусловленности: ", norm_A * norm_A1)

    print("Число ненулевых элементов: ", A.nnz)

# Расчёт предобуславливателей

if prec_flag:
    begin = time.clock()
    diags = A.diagonal()
    precond_Jacobi = (sp.sparse.diags(diags)).tocsc()
    end = time.clock()
    print("Предобуславливатель Якоби: ", end - begin)

    begin = time.clock()
    precond_ILU = sp_lg.spilu(A)
    end = time.clock()
    print("Предобуславливатель неполного LU-разложения: ", end - begin)

    begin = time.clock()
    D_min_F = sp.sparse.triu(A, format='csc')
    D_min_E = sp.sparse.tril(A, format='csc')
    D_1 = (sp.sparse.diags(1 / diags)).tocsc()
    precond_GS = D_min_E @ D_1 @ D_min_F
    end = time.clock()
    print("Предобуславливатель Гаусса-Зейделя: ", end - begin)

# Расчёт предобуславливателей при помощи LinearOperator

diags = A.diagonal()
ILU = sp_lg.spilu(A)
L = sp.sparse.tril(A, format='csr')

# Вывести вид матрицы L
if plot_flag:
    plt.spy(L, markersize=0.1)
    plt.show()


def jacobi_precond(x):
    global diags
    return x / diags


def ilu_precond(x):
    global ILU
    return ILU.solve(x)


def gs_precond(x):
    global L
    return sp_lg.spsolve_triangular(L, x, lower=True)


begin = time.clock()
Jacobi_LO = sp_lg.LinearOperator(A.shape, jacobi_precond)
end = time.clock()
Jacobi_time = end - begin

begin = time.clock()
ILU_LO = sp_lg.LinearOperator(A.shape, ilu_precond)
end = time.clock()
ILU_time = end - begin

begin = time.clock()
gs_precond = sp_lg.LinearOperator(A.shape, gs_precond)
end = time.clock()
GS_time = end - begin

print(Jacobi_time)
print(ILU_time)
print(GS_time)

exit()

# Расчёт

sol = sp.ones(A.shape[0])  # Решение
rhs = A @ sol  # Правая часть

iteration = 1
solver_name = 1
number_of_methods = 9
r_vectors = []
for i in range(0, number_of_methods):
    r_vectors.append([])


def func(x):
    global iteration, solver_name

    # Невязка для построения графиков

    if plot_r_flag:
        if solver_name == 4:
            r_vectors[solver_name].append(x)
        else:
            r_val = norm(rhs - A @ (x.reshape(rhs.shape[0], 1))) / norm(rhs)
            r_vectors[solver_name].append(r_val)

    iteration += 1


def call_solver(precond):

    global iteration, solver_name
    iteration = 1

    if solver_name == 0:
        res, info = sp_lg.bicg(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 1:
        res, info = sp_lg.bicgstab(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 2:
        res, info = sp_lg.cg(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 3:
        res, info = sp_lg.cgs(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 4:
        res, info = sp_lg.gmres(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 5:
        res, info = sp_lg.lgmres(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 6:
        res, info = sp_lg.minres(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 7:
        res, info = sp_lg.gcrotmk(A, rhs, tol=1.0e-8, maxiter=1000, M=precond, callback=func)
    elif solver_name == 8:
        res, info = sp_lg.qmr(A, rhs, tol=1.0e-8, maxiter=1000, callback=func)
    else:
        raise ValueError("Error!")

    return res, info


print("Число итераций\tВремя решения\tПолное время\tНевязка\tВремя 1 итерации\tОтклонение решения")
for i in range(1, number_of_methods):
    solver_name = i
    begin = time.clock()
    res0, info0 = call_solver(gs_precond)
    end = time.clock()

    work_time = end - begin
    full_time = work_time + GS_time
    residual = norm(rhs - A @ (res0.reshape(rhs.shape[0], 1))) / norm(rhs)
    error = norm(res0 - sol) / norm(sol)

    print("%d\t%10.10f\t%10.10f\t%10.10e\t%10.10e\t%10.10e" % (iteration, work_time, full_time, residual, work_time / iteration, error))

if plot_r_flag:
    dots = []
    for i in range(0, number_of_methods):
        dots.append(list(range(0, len(r_vectors[i]))))

    # element_0 = plt.semilogy(dots[0], r_vectors[0], label="BICG")
    element_1 = plt.semilogy(dots[1], r_vectors[1], label="BiCGStab")
    element_2 = plt.semilogy(dots[2], r_vectors[2], label="CG")
    # element_3 = plt.semilogy(dots[3], r_vectors[3], label="CGS")
    # element_4 = plt.semilogy(dots[4], r_vectors[4], label="GMRES")
    element_5 = plt.semilogy(dots[5], r_vectors[5], label="LGMRES")
    element_6 = plt.semilogy(dots[6], r_vectors[6], label="MINRES")
    element_7 = plt.semilogy(dots[7], r_vectors[7], label="GCROT")
    # element_8 = plt.semilogy(dots[8], r_vectors[8], label="QMR")

    plt.xlabel("Число итераций")
    plt.ylabel("Относительная норма невязки")
    plt.legend()

    plt.show()