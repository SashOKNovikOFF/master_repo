import numpy as np
import scipy.sparse as sp

rhs = np.zeros(3528)
x = np.zeros(3528)

def readHB(fname):
    
    global idx,ptrs,vals,rhs
    #fname='matrix.hb'
    f=open(fname,'r')
    
    
    '''
    Line 1 (A72,A8)
    Col. 1 - 72	Title (TITLE)
    Col. 73 - 80	Key (KEY)
    '''
    head = f.readline()
    title=head[:72].strip()
    key=head[72:].strip()

    '''
    Line 2:
    TOTCRD, integer, total number of data lines, (14 characters)
    PTRCRD, integer, number of data lines for pointers, (14 characters)
    INDCRD, integer, number of data lines for row or variable indices, (14 characters)
    VALCRD, integer, number of data lines for numerical values of matrix entries, (14 characters)
    RHSCRD, integer, number of data lines for right hand side vectors, starting guesses, and solutions, (14 characters)
    '''

    s = f.readline().split()
    totcrd = int(s[0])
    ptrcrd = int(s[1])
    indcrd = int(s[2])
    valcrd = int(s[3])
    rhscrd = int(s[4])

    '''
    Line 3:
    MXTYPE, matrix type (see table), (3 characters)
    blank space, (11 characters)
    NROW, integer, number of rows or variables, (14 characters)
    NCOL, integer, number of columns or elements, (14 characters)
    NNZERO, integer, number of row or variable indices. For "assembled" matrices, this is just the number of nonzero entries. (14 characters)
    NELTVL, integer, number of elemental matrix entries. For "assembled" matrices, this is 0. (14 characters)
    '''

    s = f.readline().split()
    mxtype=s[0]
    nrow=int(s[1])
    ncol=int(s[2])
    nnzero=int(s[3])
    neltvl=int(s[4])

    mxf=mxtype[0]
    mxsym=mxtype[1]
    mxasm=mxtype[2]

    '''
    Line 4:
    PTRFMT, FORTRAN I/O format for pointers, (16 characters)
    INDFMT, FORTRAN I/O format for row or variable indices, (16 characters)
    VALFMT, FORTRAN I/O format for matrix entries, (20 characters)
    RHSFMT, FORTRAN I/O format for right hand sides, initial guesses, and solutions, (20 characters)
    '''
    fmts = f.readline().split()


    '''
    Line 5: (only present if 0 < RHSCRD!)
    RHSTYP, describes the right hand side information, (3 characters)
    blank space, (11 characters)
    NRHS, integer, the number of right hand sides, (14 characters)
    NRHSIX, integer, number of row indices, (14 characters)
    '''
    if rhscrd>0:
        s = f.readline().split()
        rhstyp = s[0]
        nrhs=int(s[1])
        nrhsix=int(s[2])



    #    PTRCRD, integer, number of data lines for pointers, (14 characters)

    def readblock(f, num, nlines, dtype=int):
        res=np.zeros(num)
        k=0
        for i in range(nlines):
            s= f.readline().replace('D','E')
            fragment = np.fromstring(s,dtype=dtype,sep=' ')
            l=len(fragment)
            res[k:k+l]=fragment
            k+=l
        return res

    ptrs=readblock(f,nrow+1,ptrcrd)-1

    #INDCRD, integer, number of data lines for row or variable indices, (14 characters)
    idx=readblock(f,nnzero,indcrd)-1

    if mxf == 'C':
        raise ValueError('Complex numbers not supported for now')

    if mxf != 'P':
        #VALCRD, integer, number of data lines for numerical values of matrix entries, (14 characters)
        vals=readblock(f,nnzero,valcrd,dtype=np.float64)
    else:
        vals = np.ones(nnzero)


    #RHSCRD    
    if rhscrd>0:
        rhs = readblock(f,nrow,rhscrd,dtype=np.float64)    


    ''' 
    Each character of the MXTYPE variable specifies a separate fact about the matrix:

    R, C or P indicates that the matrix values are real, complex, or that only the pattern of nonzeroes is going to be supplied. Note that if complex arithmetic is specified, then any data vectors included in the file will also be assumed to be complex. FORTRAN I/O treats a complex number as a simple pair of real numbers. Thus, a line that records the single complex number 12+17i would look like
                12.0  17.0

    U, S, H, Z or R indicates that the matrix is symmetric, unsymmetric, Hermitian, skew symmetric, or rectangular. Each of these facts implies something about how the nonzero elements of the matrix are stored in the file.
    U: if the matrix is unsymmetric (and square), then every nonzero element of the matrix corresponds to an entry in the file.
    S: if the matrix is symmetric (which implies that it is square), (and which typically only occurs for real arithmetic), then half of the nonzero off-diagonal elements don't need to be stored in the file. A user need only specify the diagonal elements, and perhaps just those beneath the diagonal. A program reading the file must, correspondingly, assume that a value associated with one off-diagonal element should also be assigned to its corresponding transposed location.
    H: if the matrix is Hermitian, (which implies that it is square) (and which typically only occurs for complex arithmetic), then half of the nonzero off-diagonal elements don't need to be stored in the file. A user need only specify the diagonal elements, and perhaps just those beneath the diagonal. A program reading the file must, correspondingly, assume that a value associated with one off-diagonal element should also be used to assign a value to its corresponding transposed location.
    Z: if the matrix is skew symmetric, (which implies that it is square) (and which typically only occurs for real arithmetic), then the diagonal is zero, and only half of the nonzero offdiagonal elements need to be stored. (I believe that the Z code is only appropriate for a real matrix, and that the case of a skew Hermitian matrix is not provide for!)
    R: if the matrix is rectangular, then every nonzero element of the matrix must be stored. In effect, this is the same as the unsymmetric case.
    A indicates that the matrix is "assembled" (the typical case) while E indicates that the matrix is a finite element matrix that is going to be described as the "sum" of a set of smaller matrices.
    '''
    
    A=sp.csc_matrix((vals,idx,ptrs),shape=(nrow,ncol))

    if mxsym=='U':
        pass
    if mxsym=='S':
        D=sp.diags(A.diagonal())
        A = A+A.transpose()-D
    if mxsym=='Z':
        A = A-A.transpose()
    if mxsym=='H':
        D=sp.diags(A.diagonal())
        A = A+A.transpose().conj()-D
    print(rhscrd)
    f.close()
    return A