#include <cmath>
#include <iostream>
#include <vector>
#include <functional>

#include "TDM_Algorithm.hpp"

const int N = 100; // COMMENT: ����� ����� �������� ������� �������� �� ���������
const double U1 = 1.0;
const double U2 = 2.0;

std::vector<double> tdm_function(double* left, double* center, double* right, double* free) {

	TDM_Algorithm tdm(N);
	tdm.setLeftBound(left[0], center[0], free[0]);
	for (int node = 1; node < N - 1; node++)
		tdm.setValues(node, left[node], center[node], right[node], free[node]);
	tdm.setRightBound(center[N - 1], right[N - 1], free[N - 1]);
	
	tdm.solve();
	return tdm.getSolution();

};

void func_1() {

	double length = 1.0;
	double dx = length / N;

	double* left = new double[N];
	double* center = new double[N];
	double* right = new double[N];
	double* free = new double[N];

	// ����� �������
	left[0] = 1.0;
	center[0] = right[0] = 0.0;
	free[0] = U1;

	// ����������� ����
	for (int node = 1; node < N - 1; node++) {

		left[node] = 1.0 / dx;
		center[node] = -2.0 / dx;
		right[node] = 1.0 / dx;
		free[node] = 0.0;

	};

	// ������ �������
	left[N - 1] = center[N - 1] = 0.0;
	right[N - 1] = 1.0;
	free[N - 1] = U2;

	std::vector<double> solution = tdm_function(left, center, right, free);

	for (int node = 0; node < N; node++)
		std::cout << "Vec[" << node << "] = " << solution[node] << std::endl;

	delete left, center, right, free;

};

int main() {

	const double L = 2.0;                // ����� ������
	const double ksi = 0.9;              // ��������� ���� ���� ������ ksi = L1 / L2
	const double L1 = ksi / (ksi + 1.0); // ����� ������ ����� ������
	const double k1 = 110.0;             // ���������������� ������
	const double k2 = 237.0;             // ���������������� ��������
	const double len1 = 0.3;             // ����� ������� �������
	const double len2 = 0.5;             // ������ ������� �������
	
	// ����� ��������� �������
	const double alpha = 0.26; // ����������� �����������
	const double Tf = 368;     // ����������� ���������� �����

	// ������ ��������� �������
	const double q = 4.0;      // ����� ����� �������
	
	// ������� ������� S(T)
	std::function<double(double)> S_T = [](double T) { return 7.0 - T * T; };
	
	// ������� ����������� ����� ����� ������
	int N0 = N / 4; // ����� ����� ����� ����� ���������������� ���������
	double dx_1 = len1 / N0; // ��� (�� ����� ������� ������ �� ����� ������� �������)
	double dx_2 = (L1 - len1 - dx_1) / N0; // ��� (�� ����� ������� ������� �� �����)
	double dx_4 = (1.0 - len2) / N0; // ��� (�� ������ ������� ������� �� ������ ������� ������)
	double dx_3 = (len2 + dx_4 - L1) / N0; // ��� (�� ����� �� ������ ������� �������)

	std::vector<double> xNodes; // ���������� ����� �����
	std::vector<double> dxVec(N); // ����������� ������
	std::vector<double> dxSteps(N - 1); // ���������� ����� ������

	for (int i = 0; i < N0; i++)
		xNodes.push_back(i * dx_1);
	int left_heat = (int)xNodes.size();

	for (int i = 0; i < N0; i++)
		xNodes.push_back(len1 + dx_1 + i * dx_2);
	int split = (int)xNodes.size();

	for (int i = 0; i < N0; i++) {
		xNodes.push_back(L1 + i * dx_3);
	std::cout << L1 + i * dx_3 << std::endl;
	};
	int right_heat = (int)xNodes.size();
	
	for (int i = 0; i <= N0; i++)
		xNodes.push_back(len2 + i * dx_4);

	// TODO: �������� ������� ������� ����� ���������
	// TODO: �������� ������� ������� ������ � ����

	return 0;
};