#pragma once

#include <vector>

class TDM_Algorithm;

class TDM_Algorithm {

protected:

	int size;
	std::vector< std::vector<double> > matrix;
	std::vector<double> alpha;
	std::vector<double> beta;
	std::vector<double> solution;

public:

	TDM_Algorithm(int size_);

	void setLeftBound(double c0, double b0, double f0);
	void setRightBound(double aN, double cN, double fN);
	void setValues(int index, double ai, double ci, double bi, double fi);
	void solve();
	std::vector<double> getSolution();
	
};